from rest_framework import serializers

from todoapi.models import Task

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('title','description','completed','created_at')
