from django.contrib import admin
from django.urls import path , include
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('tasks/',views.tasks.as_view(), name='task_list'),
    path('posts/', views.posts.as_view(), name='posts'),
]
