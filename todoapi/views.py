from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from todoapi.models import Task, Post
from . serializers import TaskSerializer

class tasks(APIView):
    def get(self, request):
        alltasks=Task.objects.all()
        serializer=TaskSerializer(alltasks, many=True) 
        return Response(serializer.data)
    
    def post(self):
        pass


class posts(APIView):
    def get(self, request):
        alltasks=Post.objects.all()
        serializer=TaskSerializer(alltasks, many=True) 
        return Response(serializer.data)
    
    def post(self):
        pass


# Create your views here.
# @api_view(['GET', 'POST'])
def task_list(request):
    if request.method == 'GET':
        tasks= Task.objects.all()
        serializers = TaskSerializer(tasks, many=True)
        return Response(serializers.data)
    elif request.method == 'POST':
        serializers=TaskSerializer(data=request.DATA)
        if serializers.is_valid():
            serializers.save()
            return Response(serializers.data,status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializers.errors,status=status.HTTP_400_BAD_REQUEST
            )
# @api_view(['GET','PUT','DELETE'])
def task_detail(request,pk):
    try:
        task=Task.objects.get(pk=pk)
    except Task.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TaskSerializer(task)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TaskSerializer(task,data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

